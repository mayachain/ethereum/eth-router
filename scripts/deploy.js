async function main() {
  const [account] = await ethers.getSigners();
  console.log(
    `Deploying contracts with the account: ${
      account.address
    } with balance: ${ethers.utils.formatEther(await account.getBalance())}`
  );
  const Router = await ethers.getContractFactory("MAYAChain_Router");
  const mayaRouter = await Router.deploy();

  console.log("Router address:", mayaRouter.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
